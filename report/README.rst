::
  
  title: My super duper report
  author: FG-16-IU-000, Gregor von Laszewski, laszewski@gmail.com
  author: FG-16-IU-001, Author Two, author.two@email.com
  author: FG-16-IU-002, Author Three, three@email.com
  pages: 5
  status: completed/draft

::

  @TechReport{project-000, 
    author = 	 {von Laszewski, Gregor and Two, Author and Three, Author},
    title = 	 {{Put Your Title Here}},
    institution =  {Indiana University},
    year = 	 2016,
    type = 	 {use Project or Paper},
    number = 	 {project-000},
    address = 	 {Bloomington, IN 47408, U.S.A.},
    month = 	 dec,
    url={put the pdf gitlab url here/report.pdf}
  }

-------- CUT -------------------

I Thought this explanation would not be needed, but many of you just did not 
follow this example,

a) CUT everything below line 23, e.g. one line above wher you find ---- CUT ----
b) MAKE SURE YOU ONLY FILL OUT THE INFORMATION THAT IS NEEDED
c) THERE IS ONLY ONE REFERNCE IN IT, e.g. the citation for your PDF file, no other 
   url should show up. DO not invent fields that are needed such as gitlab= 
   the PDF file location is sufficient. the url must have report.pdf in it. This 
   is not the place to put your refernces that you use in your paper.
d) If you need a README.rst for your code, it should go in the code directory
e) make sure your pdf has the project number in the title and the project type.
f) do not make this mor complicated than needed. 
